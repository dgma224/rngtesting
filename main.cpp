#include <random>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <chrono>

#if defined(_OPENMP)
#include <omp.h>
#endif
#if defined(__HIPCC__)
#define __PREPROCD__ __device__
#elif defined(__NVCOMPILER) || defined(__NVCC__)
#define __PREPROCD__ __device__
#include <cuda_runtime.h>
#else
#define __PREPROCD__ 
#endif

#include "inc/xoshiro256p.h"
#include "inc/normalMethod.h"

using namespace std;


#if defined(__NVCC__) || defined(__NVCOMPILER)
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}
#elif defined(__HIPCC__)
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(hipError_t code, const char *file, int line, bool abort=true)
{
   if (code != hipSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", hipGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}
#endif



int main(int argc, char *argv[]){
	printf("starting RNG Testing\n");
	if(argc < 5){
		printf("Not enough input values\n");
		printf("Expects method, seed, Number of Values per Iteration, Number of Iterations\n");
		return -1;
	}
	int method = atoi(argv[1]);
	int seed = atoi(argv[2]);
	int numPerIter = atoi(argv[3]);
	int numIter = atoi(argv[4]);
	printf("RNG Method = %d, numPerIter = %d, numIter = %d\n", method, numPerIter, numIter);
	auto start = std::chrono::high_resolution_clock::now();
	auto stop = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop-start).count();
		
	if(method == 0){
		//use the default method for the available hardware
		//For the CPU this is mersenne twister
		//for the GPU this is the XORWOW method
		#if defined(__HIPCC__) || defined(__NVCOMPILER) || defined(__NVCC__)
		#if defined(__HIPCC__)
		hiprandStateXORWOW_t* rngStates;
		hipMalloc(&rngStates, sizeof(hiprandStateXORWOW_t) * numPerIter);
		size_t stateSize = sizeof(hiprandStateXORWOW_t) * numPerIter;
		std::cout<<"Method "<<method<<" state size of "<<stateSize<<std::endl;
		//initialize all the states
		initializeStates(rngStates, seed);
		double* gpuOutput;
		gpuOutput = hipMalloc(&gpuOutput, sizeof(double) * numPerIter);
		double* cpuOutput = (double*)malloc(sizeof(double) numPerIter);
		int threadsPerBlock = 256;
		int numBlocks = numPerIter/threadsPerBlock + 1;
		start = std::chrono::high_resolution_clock::now();
		for(int i = 0; i < numIter; i++){
			generateValues<<<numBlocks, threadsPerBlock>>>(gpuOutput, rngStates, numPerIter);
		}
		stop = std::chrono::high_resolution_clock::now();
		duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop-start).count();
		std::cout<<"Method "<<method<<" took "<<(double)duration/1000.0<<std::endl;
		hipMemcpy(cpuOutput, gpuOutput, sizeof(double) * numPerIter, hipMemcpyDeviceToHost);
		gpuErrchk(hipDeviceSynchronize());
		printf("%lf\n", output[0]);
		free(cpuOutput);
		hipFree(gpuOutput);
		hipFree(rngStates);
		#elif defined(__NVCOMPILER) || defined(__NVCC__)
		curandStateXORWOW_t* rngStates;
		cudaMalloc(&rngStates, sizeof(curandStateXORWOW_t) * numPerIter);
		size_t stateSize = sizeof(curandStateXORWOW_t) * numPerIter;
		std::cout<<"Method "<<method<<" state size of "<<stateSize<<std::endl;
		//initialize all the states
		int threadsPerBlock = 256;
		int numBlocks = numPerIter/threadsPerBlock + 1;
		std::cout<<numBlocks<<" : "<<threadsPerBlock<<std::endl;
		initializeStates<<<numBlocks, threadsPerBlock>>>(rngStates, seed, numPerIter);
		gpuErrchk(cudaDeviceSynchronize());
		double* gpuOutput;
		cudaMalloc(&gpuOutput, sizeof(double) * numPerIter);
		double* cpuOutput = (double*)malloc(sizeof(double)*numPerIter);
		start = std::chrono::high_resolution_clock::now();
		for(int i = 0; i < numIter; i++){
			generateValues<<<numBlocks, threadsPerBlock>>>(gpuOutput, rngStates, numPerIter);
		}
		stop = std::chrono::high_resolution_clock::now();
		duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop-start).count();
		std::cout<<"Method "<<method<<" took "<<(double)duration/1000.0<<std::endl;
		cudaMemcpy(cpuOutput, gpuOutput, sizeof(double) * numPerIter, cudaMemcpyDeviceToHost);
		gpuErrchk(cudaDeviceSynchronize());
		std::cout<<cpuOutput[1]<<std::endl;
		free(cpuOutput);
		cudaFree(gpuOutput);
		cudaFree(rngStates);
		#endif	
		#else
		std::mt19937_64* gens = (std::mt19937_64*)malloc(sizeof(std::mt19937_64)*numPerIter);
		std::uniform_real_distribution<double>* dists = (std::uniform_real_distribution<double>*)malloc(sizeof(std::uniform_real_distribution<double>)*numPerIter);	
		size_t stateSize =sizeof(std::mt19937_64)*numPerIter +  sizeof(std::uniform_real_distribution<double>)*numPerIter;
		std::cout<<"Method "<<method<<" state size of "<<stateSize<<std::endl;
		for(int i = 0; i < numPerIter; i++){
			gens[i].seed(seed + i);
			std::uniform_real_distribution<double> dist(0.0, 1.0);
			dists[i] = dist;
		}
		double* output = (double*)malloc(sizeof(double) * numPerIter);
		start = std::chrono::high_resolution_clock::now();
		for(int i = 0; i < numIter; i++){
			#if defined(_OPENMP)
			#pragma omp parallel for
			#endif
			for(unsigned int j = 0; j < numPerIter; j++){
				output[j] = dists[j](gens[j]);
			}
		}
		stop = std::chrono::high_resolution_clock::now();
		duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop-start).count();
		std::cout<<"Method "<<method<<" took "<<(double)duration/1000.0<<std::endl;
		printf("%lf\n", output[0]);
		free(gens);
		free(dists);
		free(output);
		#endif
	}
	else if(method == 1){
		#if defined(__HIPCC__) || defined(__NVCOMPILER) || defined(__NVCC__)

                #else
		xoshiro256p* gens = (xoshiro256p*)malloc(sizeof(xoshiro256p)*numPerIter);
		size_t stateSize = sizeof(xoshiro256p)*numPerIter;
		std::cout<<"Method "<<method<<" state size of "<<stateSize<<std::endl;
		for(int i = 0; i < numPerIter; i++){
			gens[i] = xoshiro256p();
			gens[i].init(seed, i);
		}
		double *output = (double*)malloc(sizeof(double) * numPerIter);
		start = std::chrono::high_resolution_clock::now();
		for(int i = 0; i < numIter; i++){
			#if defined(_OPENMP)
			#pragma omp parallel for
			#endif
			for(unsigned int j = 0; j < numPerIter; j++){
				output[j] = gens[j].uniform();
			}	
		}
		stop = std::chrono::high_resolution_clock::now();
		duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop-start).count();
		std::cout<<"Method "<<method<<" took "<<(double)duration/1000.0<<std::endl;
		printf("%lf\n", output[0]);
		free(gens);
		free(output);
		#endif
	}
	return 0;
}


