#include <random>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <chrono>

#if defined(_OPENMP)
#include <omp.h>
#endif
#if defined(__HIPCC__)
#define __PREPROCD__ __device__
#elif defined(__NVCOMPILER) || defined(__NVCC__)
#define __PREPROCD__ __device__
#include <cuda_runtime.h>
#else
#define __PREPROCD__
#endif

class xoshiro256p
{
public:
        __PREPROCD__ xoshiro256p() {};
        __PREPROCD__ ~xoshiro256p() {};
        __PREPROCD__ void init(unsigned long seed, unsigned int ipart);
        __PREPROCD__ uint64_t rol64(uint64_t, int);
        __PREPROCD__ uint64_t splitmix64();
        __PREPROCD__ void xorshift128_init(uint64_t);
        __PREPROCD__ uint64_t genbits();
        __PREPROCD__ double uniform();
        __PREPROCD__ double uniform(double, double);
        __PREPROCD__ double normal(double, double);
        __PREPROCD__ double maxboltz(const double);
        __PREPROCD__ double unif02pi();
        __PREPROCD__ double exponential(const double);
private:
        uint64_t seed = 0;
        uint64_t splitmix64_state;
        uint64_t xoshiro256pState[4];
        double spareRng;
        bool hasSpare = false;
};
