#include <random>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <chrono>

#if defined(_OPENMP)
#include <omp.h>
#endif
#if defined(__HIPCC__)
#include <hip/hip_runtime.h>
#include <hiprand/hiprand.h>
#include <hiprand/hiprand_kernel.h>
#define __PREPROCD__ __device__
#elif defined(__NVCOMPILER) || defined(__NVCC__)
#define __PREPROCD__ __device__
#include <cuda_runtime.h>
#include <curand.h>
#include <curand_kernel.h>
#else
#define __PREPROCD__
#endif

#if defined(__HIPCC__) || defined(__NVCOMPILER) || defined(__NVCC__)
#if defined(__HIPCC__)
__global__ void initializeStates(hiprandStateXORWOW_t* states, int seed, int num);
__global__ void generateValues(double *output, hiprandStateXORWOW_t* states, int num);
#elif defined(__NVCOMPILER) || defined(__NVCC__)
__global__ void initializeStates(curandStateXORWOW_t* states, int seed, int num);
__global__ void generateValues(double *output, curandStateXORWOW_t* states, int num);
#endif
#endif
