#ifndef NORMALMETHOD
#define NORMALMETHOD

#include "../inc/normalMethod.h"

#if defined(__HIPCC__) || defined(__NVCOMPILER) || defined(__NVCC__)
#if defined(__HIPCC__)
__global__ void initializeStates(hiprandStateXORWOW_t* states, int seed, int num){
        int tid = threadIdx.x + blockIdx.x * blockDim.x;
        hiprand_init(seed, tid, 0, &states[tid]);
}

__global__ void generateValues(double *output, hiprandStateXORWOW_t* states, int num){
        int tid = threadIdx.x + blockIdx.x * blockDim.x;
        if(tid < num){
                output[tid] = hiprand_uniform_double(&states[tid]);
        }
}
#elif defined(__NVCOMPILER) || defined(__NVCC__)
__global__ void initializeStates(curandStateXORWOW_t* states, int seed, int num){
        int tid = threadIdx.x + blockIdx.x * blockDim.x;
	printf("what the actual heck is going on\n");
        if(tid < num){
		curandStateXORWOW_t state = states[tid];
                curand_init(seed, tid, 0, &states[tid]);
		states[tid] = state;
        }
}

__global__ void generateValues(double *output, curandStateXORWOW_t* states, int num){
        int tid = threadIdx.x + blockIdx.x * blockDim.x;
        if(tid < num){
		curandStateXORWOW_t state = states[tid];
                output[tid] = curand_uniform_double(&state);
		printf("%d %lf\n", tid, output[tid]);
		states[tid] = state;
        }
}
#endif
#endif

#endif
