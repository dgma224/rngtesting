#ifndef XOSHIRO256P
#define XOSHIRO256P

#include <random>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <chrono>

#if defined(_OPENMP)
#include <omp.h>
#endif
#if defined(__HIPCC__)
#define __PREPROCD__ __device__
#elif defined(__NVCOMPILER) || defined(__NVCC__)
#define __PREPROCD__ __device__
#include <cuda_runtime.h>
#else
#define __PREPROCD__
#endif

#include "../inc/xoshiro256p.h"

__PREPROCD__ void xoshiro256p::init(unsigned long seed, unsigned int ipart){
        xorshift128_init(seed + ipart);
        uniform();
}

__PREPROCD__ uint64_t xoshiro256p::rol64(uint64_t x, int k)
{
        return (x << k) | (x >> (64 - k));
}

__PREPROCD__ uint64_t xoshiro256p::splitmix64() {
        uint64_t result = (splitmix64_state += 0x9E3779B97f4A7C15);
        result = (result ^ (result >> 30)) * 0xBF58476D1CE4E5B9;
        result = (result ^ (result >> 27)) * 0x94D049BB133111EB;
        return result ^ (result >> 31);
}

__PREPROCD__ void xoshiro256p::xorshift128_init(uint64_t seed) {
    splitmix64_state = seed; //apply the seed to that generator
        uint64_t tmp = splitmix64();
        xoshiro256pState[0] = (uint32_t)tmp;
        xoshiro256pState[1] = (uint32_t)(tmp >> 32);

        tmp = splitmix64();
        xoshiro256pState[2] = (uint32_t)tmp;
        xoshiro256pState[3] = (uint32_t)(tmp >> 32);
}

__PREPROCD__ uint64_t xoshiro256p::genbits()
{
   //using this https://en.wikipedia.org/wiki/Xorshift#xoshiro256p
        const uint64_t result = xoshiro256pState[0] + xoshiro256pState[3];
        const uint64_t t = xoshiro256pState[1] << 17;
        xoshiro256pState[2] ^= xoshiro256pState[0];
        xoshiro256pState[3] ^= xoshiro256pState[1];
        xoshiro256pState[1] ^= xoshiro256pState[2];
        xoshiro256pState[0] ^= xoshiro256pState[3];
        xoshiro256pState[2] ^= t;
        xoshiro256pState[3] = rol64(xoshiro256pState[3], 45);
        return result;
}

__PREPROCD__ static inline double DoubleFromBits(const uint64_t i){
    return (i >> 11) * 0x1.0p-53;
}

__PREPROCD__ double xoshiro256p::uniform(){
    //A random number between low and high based on the xoshiro256** algorithm
        //https://en.wikipedia.org/wiki/Xorshift#xoshiro256**
    //https://pxoshiro256p.di.unimi.it/
    uint64_t temp = genbits();
    double out = DoubleFromBits(temp);
    return out;
}

__PREPROCD__ double xoshiro256p::uniform(double low, double high){
    return uniform()*(high-low)+low;
}

__PREPROCD__ double xoshiro256p::normal(double mean, double std){
    //Generates a random value from a normal distribution using the Marsaglia Polar Method.
        //https://en.wikipedia.org/wiki/Marsaglia_polar_method
    if(hasSpare){
        hasSpare = false;
        return spareRng * std + mean;
    }
    else{
        double ts, tu, tv;
        do {
            tu = uniform(-1.0, 1.0);
            tv = uniform(-1.0, 1.0);
            ts = tu*tu + tv*tv;
        } while (ts >= 1.0 || ts == 0.0);
        ts = sqrt(-2.0*log(ts)/ts);
        spareRng = tv * ts;
        hasSpare = true;
        return mean + std * tu * ts;
    }
}

__PREPROCD__ double xoshiro256p::maxboltz(const double sqrtkT_m){
    return normal(0.0, 1.0) * sqrtkT_m;
}

__PREPROCD__ double xoshiro256p::unif02pi(){
    return uniform() * 2.0 * M_PI;
}

__PREPROCD__ double xoshiro256p::exponential(const double tc){
    return - tc * log(1.0 - uniform());
}

#if defined(__HIPCC__) || defined(__NVCOMPILER) || defined(__NVCC__)
__global__ void initializeXOSHIRO256p(xoshiro256p* rngs, unsigned int num, unsigned long seed){
        unsigned int tid = threadIdx.x + blockIdx.x * blockDim.x;
        if(tid < num){
                rngs[tid] = xoshiro256p();
                rngs[tid].init(seed, tid);
        }
}
#else
void initializeXOSHIRO256p(xoshiro256p* rngs, unsigned int num, unsigned long seed){
        #if defined(_OPENMP)
        #pragma omp parallel for
        #endif
        for(unsigned int tid = 0; tid < num; tid++){
                rngs[tid] = xoshiro256p();
                rngs[tid].init(seed, tid);
        }
}
#endif

#endif
